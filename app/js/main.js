// Initialize the application's namespace.
var Michel = {};
Michel.Pages = {};

$(function(){
  // Initialize an application instance.
  // I must receive the body element as the container.
  var app = new Michel.ModuleInitializer(
    $('[data-module]'),
    document.body
  );

  // Just start the application.
  app.init();
});

Michel.ModuleInitializer = (function(){
  function ModuleInitializer(modules, container) {
    this.modules = modules;
    this.container = $(container);
  }

  ModuleInitializer.fn = ModuleInitializer.prototype;

  ModuleInitializer.fn.init = function() {
    this.modulesInit(this.container);
  };

  ModuleInitializer.fn.modulesInit = function (container) {
    this.modules.each(function(){
      var countModules = $(this).data("module").split(" ");

      for (var i = 0; i < countModules.length; i++) {

        var module = window["Michel"][countModules[i]];

        if (typeof module === "function") {
          var initializer = new module(
            $(this),
            container
          );

          initializer.run();
        }
      }
    });
  };

  return ModuleInitializer;
})();

Michel.Enviar = (function(){
  function Enviar(element) {
    this.element = element;
  }

  Enviar.fn = Enviar.prototype;

    Enviar.fn.run = function() {
        this.element.on("click", $.proxy(this, "click"));
    };

    Enviar.fn.click = function() {
        var nome = $("#nome").val();
        var email = $("#email").val();
        var mensagem = $("#mensagem").val();
        var urlData = "&nome=" + nome + "&email=" + email + "&msg=" + mensagem;
        this.ajax(urlData);
    };

    Enviar.fn.ajax = function(urlData) {
    	console.log(urlData);
		/* Ajax */
	    $.ajax({
		type: "POST",
		url: "sendmail.php", /* endereço do script PHP */
			async: true,
			data: urlData, /* informa Url */
			success: function(data) { /* sucesso */
				//console.log(data);
				$('#envioemail').html(data);
				return true;
			},
			beforeSend: function() { /* antes de enviar */
				$('.loading').fadeIn('fast');
			},
			complete: function(){ /* completo */
				$('.loading').fadeOut('fast'); //wow!
	        }
	    });
    };

  return Enviar;
})();